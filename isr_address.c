#include <cpu32/interrupts.h>
#include "virt.h"


static void isr_addr(isr_frame_c_t *frame, unsigned long *regs) {
	if ((frame->ssw.data & 0xE3C0) == 0x0200) {
		// Type I - released write fault
		unsigned long dbuf_addr = (unsigned long)&frame->dbuf;
		unsigned long faulted_addr = frame->faulted_addr;
		if ((frame->faulted_addr & 1) && (frame->ssw.flag.siz == 2)) {
			// Unaligned word write access
			dbuf_addr += 2;
			asm volatile(
				"move.b %0@+, %1@+\n\t"
				"move.b %0@, %1@\n\t"
				: "+a"(dbuf_addr), "+a"(faulted_addr));
		} else if ((frame->faulted_addr & 3) && (frame->ssw.flag.lg)) {
			// Unaligned long write access
			asm volatile(
				"move.b %0@+, %1@+\n\t"
				"move.b %0@+, %1@+\n\t"
				"move.b %0@+, %1@+\n\t"
				"move.b %0@, %1@\n\t"
				: : "a"(dbuf_addr), "a"(faulted_addr));
		} else {
			// Don't know how to handle other cases
			isr_fatal();
		}
		frame->ssw.flag.rr = 0;
	} else if ((frame->ssw.data & 0xF200) == 0x0000) {
		// Type II - prefetch, operand, RMW, and MOVEP fault
		unsigned short *pc = (unsigned short*)frame->header.pc;
		unsigned char flags = frame->header.sr & 0xFF;
		if (virt_exec(&pc, regs, &flags))
			isr_fatal();
		frame->header.pc = (unsigned long)pc;
		frame->header.sr = (frame->header.sr & 0xFF00) | flags;
	} else if ((frame->ssw.data & 0xE100) == 0x4000) {
		// Type III - fault during MOVEM operand transfer
		// XXX: Handling not implemented
		isr_fatal();
	} else if ((frame->ssw.data & 0xE3C0) == 0x8040) {
		// Type IV - fault during exception processing
		// XXX: Handling not implemented
		isr_fatal();
	} else {
		// Unexpected case
		isr_fatal();
	}
}

static void __attribute__((__interrupt__)) _isr_addr(void) {
	asm(
		"movem.l %%d0-%%d7/%%a0-%%a7, %%sp@-\n\t"
		"move.l %%sp, %%sp@-\n\t"
		"pea %%sp@(68)\n\t"
		"jsr %0\n\t"
		"addq.l #8, %%sp\n\t"
		"movem.l %%sp@+, %%d0-%%d7/%%a0-%%a7\n\t"
		: : "m"(isr_addr)
	);
}

void enable_unaligned_access(void) {
	register_interrupt(3, _isr_addr);
}
