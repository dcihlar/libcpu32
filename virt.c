#include "virt.h"


static unsigned short ins_get_opcode(unsigned short **pc) {
	return *((*pc)++);
}

static unsigned long ins_get_signext16(unsigned short **pc) {
	unsigned long val = ins_get_opcode(pc);
	if (val & 0x8000)
		val |= 0xFFFF0000;
	return val;
}

static unsigned long ins_get_u32(unsigned short **pc) {
	unsigned long **pc32 = (unsigned long**)pc;
	return *((*pc32)++);
}

static unsigned char* oper_get(unsigned short **pc, unsigned long *regs, unsigned char mode, unsigned char reg, unsigned int size) {
	unsigned char *oper = (unsigned char*)0;

	switch (mode) {
		case 0:
			oper = (unsigned char*)&regs[reg] + 4 - size;
			break;
		case 1:
			oper = (unsigned char*)&regs[reg + 8] + 4 - size;
			break;
		case 2:
			oper = (unsigned char*)regs[reg + 8];
			break;
		case 3:
			oper = (unsigned char*)regs[reg + 8];
			regs[reg + 8] += size;
			break;
		case 4:
			regs[reg + 8] -= size;
			oper = (unsigned char*)regs[reg + 8];
			break;
		case 5:
			oper = (unsigned char*)(regs[reg + 8] + ins_get_signext16(pc));
			break;
		case 7:
			switch (reg) {
				case 0:
					oper = (unsigned char*)ins_get_signext16(pc);
					break;
				case 1:
					oper = (unsigned char*)ins_get_u32(pc);
					break;
				case 4:
					if (size == 1) {
						oper = (unsigned char*)*pc + 1;
						*pc += 2;
					} else {
						oper = (unsigned char*)*pc;
						*pc += size;
					}
					break;
			}
			break;
	}

	return oper;
}

int virt_exec(unsigned short **pc, unsigned long *regs, unsigned char *flags) {
	typedef struct {
		unsigned short ins      : 2;
		unsigned short size     : 2;
		unsigned short dst_reg  : 3;
		unsigned short dst_mode : 3;
		unsigned short src_mode : 3;
		unsigned short src_reg  : 3;
	} __attribute__((__packed__)) ins_move_t;

	unsigned short opcode = ins_get_opcode(pc);
	const ins_move_t *ins_move = (ins_move_t*)&opcode;

	if (ins_move->ins == 0) {
		unsigned int size = 1;

		switch (ins_move->size) {
			case 3: size = 2; break;
			case 2: size = 4; break;
		}

		const unsigned char *oper_src = oper_get(pc, regs, ins_move->src_mode, ins_move->src_reg, size);
		unsigned char *oper_dst = oper_get(pc, regs, ins_move->dst_mode, ins_move->dst_reg, size);

		if (!oper_src || !oper_dst)
			return -1;

		unsigned long val = 0;
		unsigned char high_byte = oper_src[0];
		oper_dst[0] = high_byte;
		val = high_byte;
		for (unsigned int i = 1; i < size; ++i) {
			unsigned char low_byte = oper_src[i];
			val = (val << 8) | low_byte;
			oper_dst[i] = low_byte;
		}

		if (ins_move->dst_mode != 1) {
			*flags &= ~0x0F;
			if (high_byte & 0x80) {
				*flags |= 0x08;
			}
			if (!val) {
				*flags |= 0x04;
			}
		}
	} else {
		// don't know how to handle this instruction
		return -1;
	}

	return 0;
}
