#include <cpu32/interrupts.h>


static isr_t vect[256];


void isr_fatal(void) {
	for (;;) bgnd();
}

static void isr_fault(isr_frame_header_t *frame) {
	isr_fatal();
}

static void __attribute__((__interrupt__)) _isr_fault(void) {
	asm(
		"move.l %%sp, %%sp@-\n\t"
		"jsr %0\n\t"
		"addq.l #4, %%sp\n\t"
		: : "m"(isr_fault)
	);
}

void init_interrupts(void) {
	for (int i = 2; i < 254; ++i)
		vect[i] = _isr_fault;
	set_vbr(vect);
}

int register_interrupt(int num, isr_t func) {
	if (num < 64 || num > 255) {
		return -1;
	}
	vect[num] = func;
	return 0;
}
