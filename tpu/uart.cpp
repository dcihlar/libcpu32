#include <cpu32/tpu/uart.hpp>
#include <cpu32/tpu/tpu.hpp>
#include <cpu32/sim.h>


namespace tpu {

void UARTBase::update_baudrate() {
	param().match_rate = 16777200/4/baudrate;
}

void UARTBase::init(Priority priority) {
	update_baudrate();
	param().data_size = 8;
	clear_interrupt_flag();
	set_function(Function::UART);
	set_host_sequence(TPU_HSQR_UART_NO_PARITY);
	Channel::init(priority);
}

void UARTBase::set_baudrate(unsigned int baudrate) {
	this->baudrate = baudrate;
	update_baudrate();
}

void UARTTx::init(Priority priority) {
	param().flags_data = 0x8000;	// set TDRE
	set_host_service_request(TPU_HSRR_UART_TRANSMIT);
	UARTBase::init(priority);
}

void UARTRx::init(Priority priority) {
	set_host_service_request(TPU_HSRR_UART_RECEIVE);
	UARTBase::init(priority);
}

size_t UARTTxPolling::write(const void *data, size_t size) {
	const uint8_t *bdata = (const uint8_t*)data;

	clear_interrupt_flag();
	for (size_t i = 0; i < size; ++i) {
		UARTTx::write(bdata[i]);
		while (!get_interrupt_flag())
			watchdog_feed();
		clear_interrupt_flag();
	}

	return size;
}

size_t UARTRxPolling::read(void *data, size_t size) {
	uint8_t *bdata = (uint8_t*)data;
	for (size_t i = 0; i < size; ++i) {
		while (!get_interrupt_flag())
			watchdog_feed();
		clear_interrupt_flag();
		bdata[i] = UARTRx::read();
	}
	return size;
}

void UARTTxIsr::init(Priority priority) {
	UARTTx::init(priority);
	attach_interrupt();
}

void UARTRxIsr::init(Priority priority) {
	UARTRx::init(priority);
	attach_interrupt();
}

size_t UARTTxIsr::write(const void *data, size_t size) {
	const uint8_t *bdata = (const uint8_t*)data;
	size_t towrsz = size;
	while (towrsz > 0) {
		const size_t blkwrsz = buf.write(bdata, towrsz);
		if (!busy) {
			busy = true;
			on_interrupt();
		}
		bdata += blkwrsz;
		towrsz -= blkwrsz;
		watchdog_feed();
	}
	return size;
}

void UARTTxIsr::flush() {
	while (busy)
		watchdog_feed();
}

void UARTTxIsr::on_interrupt() {
	uint8_t data;
	Channel::on_interrupt();
	if (buf.get(data)) {
		UARTTx::write(data);
	} else {
		busy = false;
	}
}

size_t UARTRxIsr::read(void *data, size_t size) {
	uint8_t *bdata = (uint8_t*)data;
	size_t tordsz = size;
	while (tordsz > 0) {
		const size_t blkrdsz = buf.read(bdata, tordsz);
		bdata += blkrdsz;
		tordsz -= blkrdsz;
		watchdog_feed();
	}
	return size;
}

void UARTRxIsr::on_interrupt() {
	uint8_t data = UARTRx::read();
	Channel::on_interrupt();
	buf.put(data);
}

}
