#include <cpu32/tpu/channel.hpp>
#include <cpu32/tpu/tpu.h>
#include <cpu32/interrupts.h>

namespace tpu {

typedef void (*int_entry_t)();

Channel* Channel::registered_channels[16];

template<int ch>
static void __attribute__((__interrupt__)) int_entry_impl() {
	Channel::registered_channels[ch]->on_interrupt();
}

static constexpr int_entry_t int_entry[16] = {
	int_entry_impl<0>,
	int_entry_impl<1>,
	int_entry_impl<2>,
	int_entry_impl<3>,
	int_entry_impl<4>,
	int_entry_impl<5>,
	int_entry_impl<6>,
	int_entry_impl<7>,
	int_entry_impl<8>,
	int_entry_impl<9>,
	int_entry_impl<10>,
	int_entry_impl<11>,
	int_entry_impl<12>,
	int_entry_impl<13>,
	int_entry_impl<14>,
	int_entry_impl<15>
};

void Channel::attach_interrupt() {
	const uint8_t vect_base = TPU.ticr;
	register_interrupt(vect_base + ch, int_entry[ch]);
	clear_interrupt_flag();
	enable_interrupts();
}

}
