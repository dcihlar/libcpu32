#ifndef _CPU32_PIT_H_
#define _CPU32_PIT_H_

#include "regbase.h"

struct _cpu32_pit_t {
	unsigned short control;
	unsigned short timing;
} __attribute__((__packed__));

#define PIT			UNIT(struct _cpu32_pit_t, 0xA22u)

#endif
