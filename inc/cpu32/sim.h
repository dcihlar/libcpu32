#ifndef _CPU32_SIM_H_
#define _CPU32_SIM_H_

#include "regbase.h"

struct _cpu32_csbo_t {
	unsigned short base;
	unsigned short option;
} __attribute__((__packed__));

struct _cpu32_csctrl_t {
	unsigned short par0;
	unsigned short par1;
	struct _cpu32_csbo_t boot;
	struct _cpu32_csbo_t cs[11];
} __attribute__((__packed__));


#define SIMCR		REG16(0xA00u)
#define SIMTR		REG16(0xA02u)
#define SYNCR		REG16(0xA04u)
#define	RSR			REG8(0xA07u)
#define SIMTRE		REG16(0xA08u)
#define SYPCR		REG8(0xA21u)
#define SWSR		REG8(0xA27u)
#define CSCTRL		UNIT(struct _cpu32_csctrl_t, 0xA44u)


static inline void watchdog_feed(void) {
	SWSR = 0x55;
	SWSR = 0xAA;
}

#endif
