#ifndef _CPU32_IO_H_
#define _CPU32_IO_H_

#include "regbase.h"

struct _cpu32_port_t {
	unsigned char data __attribute__((aligned(2)));	// data register
	unsigned char ddr  __attribute__((aligned(2)));	// data direction register
	unsigned char par  __attribute__((aligned(2)));	// pin assignment register
};

#define PORTE		UNIT(struct _cpu32_port_t, 0xA13u)
#define PORTF		UNIT(struct _cpu32_port_t, 0xA1Bu)
#define PORTC		REG8(0xA41u)

#endif
