#ifndef _CPU32_TPU_CIRCBUF_HPP_
#define _CPU32_TPU_CIRCBUF_HPP_

#include <algorithm>
#include <cstddef>
#include <cstdint>

namespace tpu {

template<size_t size, typename T = uint8_t>
class CircBuff {
	public:
		constexpr CircBuff()
			: wr_ofs(0), rd_ofs(0)
		{}

		constexpr bool is_empty() const {
			return rd_ofs == wr_ofs;
		}

		constexpr bool is_full() const {
			return ((wr_ofs + 1) % size) == rd_ofs;
		}

		constexpr size_t get_size_free() const {
			return (rd_ofs - wr_ofs - 1) % size;
		}

		constexpr size_t get_size_used() const {
			return (wr_ofs - rd_ofs) % size;
		}

		size_t write(const T *data, size_t sz) {
			sz = std::min(get_size_free(), sz);
			for (size_t i = 0; i < sz; ++i) {
				buf[wr_ofs] = data[i];
				advance(wr_ofs);
			}
			return sz;
		}

		size_t read(T *data, size_t sz) {
			sz = std::min(get_size_used(), sz);
			for (size_t i = 0; i < sz; ++i) {
				data[i] = buf[rd_ofs];
				advance(rd_ofs);
			}
			return sz;
		}

		bool get(T &data) {
			if (is_empty())
				return false;
			data = buf[rd_ofs];
			advance(rd_ofs);
			return true;
		}

		bool put(const T &data) {
			if (is_full())
				return false;
			buf[wr_ofs] = data;
			advance(wr_ofs);
			return true;
		}

	private:
		T buf[size];
		volatile size_t wr_ofs;
		volatile size_t rd_ofs;

		static void advance(volatile size_t &ofs) {
			ofs = (ofs + 1) % size;
		}
};

}

#endif
