#ifndef _CPU32_TPU_CHANNEL_HPP_
#define _CPU32_TPU_CHANNEL_HPP_

#include <cstdint>
#include "tpu.hpp"

namespace tpu {

class Channel {
	public:
		enum class Function : uint8_t {
			HALLD = 8,
			COMM,
			NITC,
			UART,
			FQM,
			TSM,
			QOM,
			PTA,
		};

		enum class Priority : uint8_t {
			Disabled = 0,
			Low,
			Middle,
			High
		};

		constexpr Channel(uint8_t id)
			: ch(id)
		{}

		auto get_channel() const { return ch; }

	protected:
		const uint8_t ch;

		void set_function(Function function) { set_custom_function((uint8_t)function); }
		void set_custom_function(uint8_t function) { Core.cfsr[ch] = function; }
		void set_host_sequence(uint8_t hsq) { Core.hsqr[ch] = hsq; }
		void set_host_service_request(uint8_t hsr) { Core.hsrr[ch] = hsr; }
		void set_priority(Priority priority) { Core.cpr[ch] = (uint8_t)priority; }
		bool get_interrupt_flag() const { return Core.cisr[ch]; }
		void clear_interrupt_flag() { Core.cisr[ch] = 0; }
		volatile _cpu32_tpu_param_t& param() const { return Core.param[ch]; }

		void init(Priority priority = Priority::Middle) {
			registered_channels[ch] = this;
			set_priority(priority);
		}

		void attach_interrupt();
		void enable_interrupts() { Core.cier[ch] = true; }
		void disable_interrupts() { Core.cier[ch] = false; }
		virtual void on_interrupt() {
			clear_interrupt_flag();
		}

	private:
		static Channel* registered_channels[16];

	template<int ch>
	friend void int_entry_impl();
};

}

#endif
