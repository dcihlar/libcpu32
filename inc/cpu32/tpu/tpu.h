#ifndef _CPU32_TPU_H_
#define _CPU32_TPU_H_

#include "../regbase.h"

#define TPU_FUNCTION_UART			0xB
#define TPU_HSQR_UART_NO_PARITY		0
#define TPU_HSQR_UART_EVEN_PARITY	2
#define TPU_HSQR_UART_ODD_PARITY	3
#define TPU_HSRR_UART_TRANSMIT		3
#define TPU_HSRR_UART_RECEIVE		2
#define TPU_CH_PRIORITY_OFF			0
#define TPU_CH_PRIORITY_LOW			1
#define TPU_CH_PRIORITY_MIDDLE		2
#define TPU_CH_PRIORITY_HIGH		3

union _cpu32_tpu_param_t {
	unsigned short raw[8];
	struct {
		unsigned short parity;
		unsigned short match_rate;
		unsigned short flags_data;
		unsigned short data_size;
		unsigned short actual_bit_count;
		unsigned short shift_register;
	} uart;
};

union _cpu32_tpu_ch1_t {
	unsigned short data;
	struct {
		unsigned short ch15 : 1;
		unsigned short ch14 : 1;
		unsigned short ch13 : 1;
		unsigned short ch12 : 1;
		unsigned short ch11 : 1;
		unsigned short ch10 : 1;
		unsigned short ch9  : 1;
		unsigned short ch8  : 1;
		unsigned short ch7  : 1;
		unsigned short ch6  : 1;
		unsigned short ch5  : 1;
		unsigned short ch4  : 1;
		unsigned short ch3  : 1;
		unsigned short ch2  : 1;
		unsigned short ch1  : 1;
		unsigned short ch0  : 1;
	};
};

union _cpu32_tpu_ch2_t {
	unsigned short data[2];
	unsigned long long_data;
	struct {
		unsigned short ch15 : 2;
		unsigned short ch14 : 2;
		unsigned short ch13 : 2;
		unsigned short ch12 : 2;
		unsigned short ch11 : 2;
		unsigned short ch10 : 2;
		unsigned short ch9  : 2;
		unsigned short ch8  : 2;
		unsigned short ch7  : 2;
		unsigned short ch6  : 2;
		unsigned short ch5  : 2;
		unsigned short ch4  : 2;
		unsigned short ch3  : 2;
		unsigned short ch2  : 2;
		unsigned short ch1  : 2;
		unsigned short ch0  : 2;
	};
};

union _cpu32_tpu_ch4_t {
	unsigned short data[4];
	unsigned long long_data[2];
	struct {
		unsigned short ch15 : 4;
		unsigned short ch14 : 4;
		unsigned short ch13 : 4;
		unsigned short ch12 : 4;
		unsigned short ch11 : 4;
		unsigned short ch10 : 4;
		unsigned short ch9  : 4;
		unsigned short ch8  : 4;
		unsigned short ch7  : 4;
		unsigned short ch6  : 4;
		unsigned short ch5  : 4;
		unsigned short ch4  : 4;
		unsigned short ch3  : 4;
		unsigned short ch2  : 4;
		unsigned short ch1  : 4;
		unsigned short ch0  : 4;
	};
};

struct _cpu32_tpu_t {
	unsigned short mcr;
	unsigned short tcr;
	unsigned short dscr;
	unsigned short dssr;
	unsigned short ticr;
	union _cpu32_tpu_ch1_t cier;
	union _cpu32_tpu_ch4_t cfsr;
	union _cpu32_tpu_ch2_t hsqr;
	union _cpu32_tpu_ch2_t hsrr;
	union _cpu32_tpu_ch2_t cpr;
	union _cpu32_tpu_ch1_t cisr;
	unsigned short lr;
	unsigned short sglr;
	unsigned short dcnr;
	union _cpu32_tpu_param_t param[16] __attribute__((aligned(256)));
};


#define TPU_BASE	0xE00u
#define TPU			UNIT(struct _cpu32_tpu_t, TPU_BASE)

#endif
