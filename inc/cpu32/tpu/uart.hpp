#ifndef _CPU32_TPU_UART_HPP_
#define _CPU32_TPU_UART_HPP_

#include <cstddef>
#include <cstdint>

#include "channel.hpp"
#include "circbuf.hpp"


namespace tpu {

class UARTBase : public Channel {
	public:
		constexpr UARTBase(uint8_t channel, unsigned int baudrate = 9600)
			: Channel(channel), baudrate(baudrate)
		{}

		void init(Priority priority = Priority::Middle);
		void set_baudrate(unsigned int baudrate);
		constexpr uint8_t get_channel() const { return ch; }

	protected:
		auto& param() { return Channel::param().uart; }

	private:
		unsigned int baudrate;

		void update_baudrate();
};

class UARTTx : public UARTBase {
	public:
		using UARTBase::UARTBase;
		void init(Priority priority = Priority::Middle);
		virtual size_t write(const void *data, size_t size) = 0;
	protected:
		void write(uint8_t data) { param().flags_data = data; }
};

class UARTRx : public UARTBase {
	public:
		using UARTBase::UARTBase;
		void init(Priority priority = Priority::Middle);
		virtual size_t read(void *data, size_t size) = 0;
		virtual size_t available() = 0;
	protected:
		uint8_t read() { return param().flags_data; }
};

class UARTTxPolling : public UARTTx {
	public:
		using UARTTx::UARTTx;
		size_t write(const void *data, size_t size) override;
};

class UARTRxPolling : public UARTRx {
	public:
		using UARTRx::UARTRx;
		size_t read(void *data, size_t size) override;
		size_t available() override { return get_interrupt_flag(); }
};

class UARTTxIsr : public UARTTx {
	public:
		using UARTTx::UARTTx;
		void init(Priority priority = Priority::Middle);
		size_t write(const void *data, size_t size) override;
		void flush();

	private:
		CircBuff<64> buf;
		volatile bool busy = false;

		void on_interrupt() override;
};

class UARTRxIsr : public UARTRx {
	public:
		using UARTRx::UARTRx;
		void init(Priority priority = Priority::Middle);
		size_t read(void *data, size_t size) override;
		size_t available() override { return buf.get_size_used(); }
	private:
		CircBuff<64> buf;
		void on_interrupt() override;
};

class IUART {
	public:
		virtual void set_baudrate(unsigned int baudrate) = 0;
		virtual size_t write(const void *data, size_t size) = 0;
		virtual size_t read(void *data, size_t size) = 0;
		virtual void flush() = 0;
		virtual size_t available() = 0;
		virtual uint8_t get_tx_channel() const = 0;
		virtual uint8_t get_rx_channel() const = 0;
};

class UARTPolling : public IUART, public UARTTxPolling, public UARTRxPolling {
	public:
		constexpr UARTPolling(uint8_t txch, uint8_t rxch, unsigned int baudrate = 9600)
			: UARTTxPolling(txch, baudrate), UARTRxPolling(rxch, baudrate)
		{}

		void init(Priority priority = Priority::Middle) {
			UARTTxPolling::init(priority);
			UARTRxPolling::init(priority);
		}

		void set_baudrate(unsigned int baudrate) override {
			UARTTxPolling::set_baudrate(baudrate);
			UARTRxPolling::set_baudrate(baudrate);
		}

		size_t write(const void *data, size_t size) override {
			return UARTTxPolling::write(data, size);
		}

		size_t read(void *data, size_t size) override {
			return UARTRxPolling::read(data, size);
		}

		void flush() override {}

		size_t available() override {
			return UARTRxPolling::available();
		}

		uint8_t get_tx_channel() const {
			return UARTTxPolling::get_channel();
		}

		uint8_t get_rx_channel() const {
			return UARTRxPolling::get_channel();
		}
};

class UARTIsr : public IUART, public UARTTxIsr, public UARTRxIsr {
	public:
		constexpr UARTIsr(uint8_t txch, uint8_t rxch, unsigned int baudrate = 9600)
			: UARTTxIsr(txch, baudrate), UARTRxIsr(rxch, baudrate)
		{}

		void init(Priority priority = Priority::Middle) {
			UARTTxIsr::init(priority);
			UARTRxIsr::init(priority);
		}

		void set_baudrate(unsigned int baudrate) override {
			UARTTxIsr::set_baudrate(baudrate);
			UARTRxIsr::set_baudrate(baudrate);
		}

		size_t write(const void *data, size_t size) override {
			return UARTTxIsr::write(data, size);
		}

		size_t read(void *data, size_t size) override {
			return UARTRxIsr::read(data, size);
		}

		void flush() override {
			UARTTxIsr::flush();
		}

		size_t available() override {
			return UARTRxIsr::available();
		}

		uint8_t get_tx_channel() const {
			return UARTTxIsr::get_channel();
		}

		uint8_t get_rx_channel() const {
			return UARTRxIsr::get_channel();
		}
};

}

#endif
