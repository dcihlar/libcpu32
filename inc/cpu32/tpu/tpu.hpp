#ifndef _CPU32_TPU_HPP_
#define _CPU32_TPU_HPP_

#include <cstdint>
#include <cstddef>

#include "tpu.h"


namespace tpu {

template <typename T>
class _MemElem {
	public:
		constexpr explicit _MemElem(uintptr_t p_data)
			: p_data(p_data)
		{}

		volatile T* ptr() const {
			return reinterpret_cast<volatile T*>(p_data);
		}

		operator volatile T&() const {
			return *ptr();
		}

		_MemElem& operator=(const T &data) {
			*ptr() = data;
			return *this;
		}

		const _MemElem& operator=(const T &data) const {
			*ptr() = data;
			return *this;
		}

	private:
		uintptr_t p_data;
};

template <typename T>
class _MemArray {
	public:
		constexpr explicit _MemArray(uintptr_t p_data)
			: p_data(p_data)
		{}

		_MemElem<T> operator[](size_t ofs) const {
			return _MemElem<T>{p_data + ofs * sizeof(T)};
		}

	private:
		uintptr_t p_data;
};

template<int field_size, typename T = uint16_t>
class _BitField {
	public:
		constexpr _BitField(const _MemElem<uint16_t> &data, unsigned int ofs)
			: data(data), ofs(ofs)
		{}

		constexpr _BitField(uintptr_t p_data, unsigned int ofs)
			: data(p_data), ofs(ofs)
		{}

		operator T() const {
			return (data & get_mask()) >> ofs;
		}

		_BitField& operator=(T val) {
			const uint16_t mask = get_mask();
			data = (data & ~mask) | (((uint16_t)val << ofs) & mask);
			return *this;
		}

	private:
		const _MemElem<uint16_t> data;
		unsigned int ofs;

		constexpr uint16_t get_mask() const {
			return ((1u<<field_size)-1u) << ofs;
		}
};

template <int field_size>
class _BitFieldArray {
	public:
		constexpr _BitFieldArray(uintptr_t p_data)
			: data(p_data)
		{}

		_BitField<field_size> operator[](unsigned int ofs) const {
			return {
				data[field_size - 1 - ofs / (16 / field_size)],
	  			(ofs % (16 / field_size)) * field_size
			};
		}

		void clear() const {
			data[0] = 0;
			if constexpr (field_size >= 2) {
				data[1] = 0;
			}
			if constexpr (field_size >= 4) {
				data[2] = 0;
				data[3] = 0;
			}
		}

	private:
		const _MemArray<uint16_t> data;
};

#define _TPU_FIELD_ADDR(name)	(BASE_ADDR(TPU_BASE) + offsetof(_cpu32_tpu_t, name))

class CCore {
	public:
		enum class Prescaler : uint8_t {
			div1 = 0,
			div2,
			div4,
			div8
		};

		_BitField<1, bool> stop{_TPU_FIELD_ADDR(mcr), 15};
		_BitField<2, Prescaler> tcr1p{_TPU_FIELD_ADDR(mcr), 13};
		_BitField<2, Prescaler> tcr2p{_TPU_FIELD_ADDR(mcr), 11};
		_BitField<4, uint8_t> iarb{_TPU_FIELD_ADDR(mcr), 0};
		_BitField<3, uint8_t> cirl{_TPU_FIELD_ADDR(ticr), 8};
		_BitField<4, uint8_t> cibv{_TPU_FIELD_ADDR(ticr), 4};
		_BitFieldArray<1> cier{_TPU_FIELD_ADDR(cier.data)};
		_BitFieldArray<1> cisr{_TPU_FIELD_ADDR(cisr.data)};
		_BitFieldArray<4> cfsr{_TPU_FIELD_ADDR(cfsr.data)};
		_BitFieldArray<2> hsqr{_TPU_FIELD_ADDR(hsqr.data)};
		_BitFieldArray<2> hsrr{_TPU_FIELD_ADDR(hsrr.data)};
		_BitFieldArray<2> cpr{_TPU_FIELD_ADDR(cpr.data)};
		_MemArray<_cpu32_tpu_param_t> param{_TPU_FIELD_ADDR(param)};

		void disable_all() const {
			TPU.cpr.long_data = 0;
		}

		void configure(Prescaler tcr1p, bool fast = true, bool supervisor_only = true) const {
			TPU.mcr = ((uint16_t)tcr1p << 13) |
			          (1u << 9) |							// T2CG
					  (supervisor_only ? (1u << 7) : 0) |
					  (fast ? (1u << 6) : 0);				// PSCK
		}

		void enable_interrupts(uint8_t irq_level, uint8_t vector_base = 64, uint8_t iarb = 0xf) const {
			cier.clear();
			cisr.clear();
			TPU.mcr = (TPU.mcr & ~0xf) | (iarb & 0xf);
			TPU.ticr = ((irq_level & 7) << 8) | (vector_base & 0xf0);
		}
};

static constexpr CCore Core;

}

#endif
