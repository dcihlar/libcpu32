#ifndef _CPU32_REGBASE_H_
#define _CPU32_REGBASE_H_

#define CTRL_BASE			0xFFF000u
#define BASE_ADDR(addr)		(CTRL_BASE | (addr))
#define UNIT(type, addr)	(*(volatile type*)BASE_ADDR(addr))
#define REG16(addr)			UNIT(unsigned short, addr)
#define REG8(addr)			UNIT(unsigned char, addr)

#endif
