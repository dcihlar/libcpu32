#ifndef _CPU32_CORE_H_
#define _CPU32_CORE_H_

static inline void bgnd() {
	asm("bgnd");
}

static inline unsigned short get_sr() {
	unsigned short sr;
	asm volatile("move.w %%sr, %0" : "=r"(sr));
	return sr;
}

static inline void set_sr(unsigned short sr) {
	asm("move.w %0, %%sr" : : "ir"(sr) : "cc");
}

#endif
