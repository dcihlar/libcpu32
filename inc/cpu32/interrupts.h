#ifndef _CPU32_INTERRUPTS_H_
#define _CPU32_INTERRUPTS_H_

#include "core.h"


typedef struct {
	unsigned short sr;
	unsigned long pc;
	unsigned short format : 4;
	unsigned short offset : 12;
} __attribute__((__packed__)) isr_frame_header_t;

typedef struct {
	isr_frame_header_t header;
	unsigned long faulted_addr;
	unsigned long dbuf;
	unsigned long current_pc;
	unsigned short count;
	union {
		unsigned short data;
		struct {
			unsigned short tp : 1;		// BERR frame type
			unsigned short mv : 1;		// MOVEM in progress
			unsigned short zero : 1;	// Always 0
			unsigned short tr : 1;		// Trace pending
			unsigned short b1 : 1;		// Breakpoint channel 1 pending
			unsigned short b0 : 1;		// Breakpoint channel 0 pending
			unsigned short rr : 1;		// Rerun write cycle after RTE
			unsigned short rm : 1;		// Faulted cycle was read-modify-write
			unsigned short in : 1;		// Instruction/other
			unsigned short rw : 1;		// Read/write of faulted bus cycle
			unsigned short lg : 1;		// Original operand size was long word
			unsigned short siz : 2;		// Remaining size of faulted bus cycle
			unsigned short func : 3;	// Function code of faulted bus cycle
		} flag;
	} ssw;
} __attribute__((__packed__)) isr_frame_c_t;

typedef void (*isr_t)(void);


static inline void mask_interrupts(unsigned char ipl) {
	unsigned short sr = get_sr();
	sr = (sr & ~0x0700) | ((unsigned short)(ipl & 7) << 8);
	set_sr(sr);
}

static inline void disable_all_interrupts() {
	asm("ori.w #0x0700, %sr");
}

static inline void enable_all_interrupts() {
	asm("andi.w #~0x0700, %sr");
}

static inline void* get_vbr(void) {
    void *vbr;
    asm volatile("movec %%vbr, %0" : "=ad"(vbr));
    return vbr;
}

static inline void set_vbr(isr_t vbr[256]) {
    asm("movec %0, %%vbr" : : "ad"(vbr));
}

#ifdef __cplusplus
extern "C" {
#endif

void isr_fatal(void);
void init_interrupts(void);
int register_interrupt(int num, isr_t func);
void enable_unaligned_access(void);

#ifdef __cplusplus
}
#endif

#endif
