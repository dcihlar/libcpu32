#ifndef _CPU32_VIRT_H_
#define _CPU32_VIRT_H_

int virt_exec(unsigned short **pc, unsigned long *regs, unsigned char *flags);

#endif
